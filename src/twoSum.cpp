#include "iostream"
#include <unordered_map>
#include "vector"
class Solution {
public:
    std::vector<int> twoSum(std::vector<int>& nums, int target) {
        std::unordered_map<int, int> res;
        for(int i=0; i< nums.size(); i++){
            auto found = res.find(nums[i]);
            if(found == res.end()){
                res.emplace(std::make_pair(target - nums[i], i));
            }
            else{
                return std::vector<int>(found->second, i);
            }
        }
        return {};
    }


};
int main(){
    auto solution = new Solution();
    std::vector<int> nums = {1,2,3,4};
    solution->twoSum(nums, 6);
    return 0;
}