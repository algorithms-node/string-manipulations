//
// Created by nikhil on 2/27/23.
//


#include <string>
#include "iostream"
#include "unordered_set"


class Solution {
public:
    int lengthOfLongestSubstring(std::string s) {
        std::unordered_set<char> v;
        int i=0, j=0;
        int max_len = 0;
        while(i < s.length()){
           if(v.find(s[i]) == v.end()){
               v.insert(s[i]);
               i++;
               max_len = std::max(max_len, i-j);
           }
           else{
               v.erase(s[j]);
               j++;
           }
        }
        std::cout << max_len << std::endl;

    }
};
int main(){
    auto solution = new Solution();
    std::string problem = "pwwkew";
    solution->lengthOfLongestSubstring(problem);
    return 0;
}