#include <bits/stdc++.h>

using namespace std;

class AddingStack {
public:
    std::vector<int> stack, incr;
    long sum_tot = 0;
    void push(int v) {
        // Complete the function below:
        sum_tot += v;
        stack.emplace_back(v);
        incr.emplace_back(0);
    }

    void pop() {
        // Complete the function below:
        auto to_pop = stack.back();
        auto to_pop_inc = incr.back();
        sum_tot -= to_pop;
        sum_tot -= to_pop_inc;
        stack.pop_back();
        incr.pop_back();
    }

    void inc(int i, int v) {
        // Complete the function below:
        for(int k=0;k<i;k++)
        {
            incr[k] = incr[k] + v;
        }
        sum_tot += i*v;
    }

    bool empty() {
        // Complete the function below:
        if(stack.empty())
            return true;
        else
            return false;
    }

    long peek() {
        // Complete the function below:
        return stack.back() + incr.back();
    }

    long sum() {
        // Complete the function below:
        return sum_tot;
    }
};
int main()
{
    int operations_size = 0;
    cin >> operations_size;
    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    AddingStack stack;
    for(int i = 0; i < operations_size; i++) {
        string operations_item;
        getline(cin, operations_item);

        istringstream command_stream(operations_item);

        string command;
        command_stream >> command;
        if (command == "push") {
            int value;
            command_stream >> value;
            stack.push(value);
        } else if (command == "pop") {
            stack.pop();
        }
        else if (command == "inc") {
            int i, v;
            command_stream >> i >> v;
            stack.inc(i, v);
        }

        if (stack.empty()) {
            cout << "EMPTY" << endl;
        } else {
            cout << stack.peek() << " " << stack.sum() << endl;
        }
    }

    return 0;
}
