//
// Created by nikhil on 3/27/23.
//

#include <iostream>

/*
* easier to calculate leap year using this than to check every month for every year
*/
bool isLeap(int year){
    return (year%4==0 && year%100!=0) || (year%400==0);
}

int DaysInMonth(int month, int year){
    if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12) return 31;
    if(month==2) return isLeap(year)?29:28;
    return 30;
}

/*
* start from beginning of year 1 and calculate days till year provided
*/
int daysTillDate(int year1, int year, int month, int day){
    int numDays = 0;
    for(int y=year1;y<year;y++) {
        numDays+=isLeap(y)?366:365;
    }
    for(int m=1; m < month; m++){
        numDays += DaysInMonth(m, year);
    }
    return numDays+day;
}

/*
* calculate days from year1 beginning to year 1 & year1 beginning to year2. Diff of that should give us number of days
*/
int DaysBetween(int year1, int month1, int day1, int year2, int month2, int day2) {

    int numDaysTilld1 = daysTillDate(year1, year1, month1, day1);
    int numDaysTilld2 = daysTillDate(year1, year2, month2, day2);
    return numDaysTilld2 - numDaysTilld1;

}



