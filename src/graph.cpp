#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

/*
 * E1 INVALID INPUT FORMAT
 * E2 DUPLICATE PAIR
 * E3 PARENT HAS MORE THAN TWO CHILDREN
 * E4 MULTIPLE ROOTS
 * E5 INPUT CONTAINS CYCLE
 * */


constexpr char NODE_DELIMITER = ',';
constexpr char TAG_OPEN = '(';
constexpr char TAG_CLOSE = ')';
constexpr char NULL_CHAR='\0';

const char *parent = "\0";
const char *child = "\0";

class Node{
public:
    char *data;
    Node *left;
    Node *right;
    Node(char *data){
        this->data = data;
        this->left = nullptr;
        this->right = nullptr;
    }
};


/*
 * B-Tree which can hold map of orders */
class Btree{
public:
    Node *root;
    Btree(){
        this->root = nullptr;
    }

    // insert un ordered binary tree. Only condition is to have 2 children
    void insert_node(char *parent, char *leaf_node){
        auto find_parent = this->find(this->root, parent);
        auto find_child = this->find(this->root,leaf_node);
        if(find_child != nullptr && find_parent != nullptr){
            auto find_parent = this->find_parent(this->root, this->root, leaf_node);
            if(strcmp(find_parent->data, leaf_node) == 0){
                printf("E2"); // duplicate child
                ::exit(0);
            }
            else{
                printf("E5"); // Cycle
                ::exit(0);
            }

        }
        if(find_child != nullptr && find_parent == nullptr){
            // insert at root
            auto *new_node = new Node(parent);
            new_node->left = find_child;
            this->root = new_node;
        }
        else if(find_child == nullptr && find_parent != nullptr){
            if(find_parent->left == nullptr){
                auto *new_node = new Node(leaf_node);
                find_parent->left = new_node;
            }
            else if(find_parent->right == nullptr){
                auto *new_node = new Node(leaf_node);
                find_parent->right = new_node;
            }
            else{
                printf("E3"); // Invalid binary tree
                ::exit(0);
            }
        }
        else{
            printf("E4"); // No parent found
            ::exit(0);
        }
    };

    // insert at root
    void insert_root(char *leaf){
        if(this->root == nullptr){
            auto *leaf_node = new Node(leaf);
            this->root = leaf_node;
        }
    };

    //  find a particular leaf node
    Node* find(Node *node, char *leaf){
        if(node == nullptr){
            return nullptr;
        }
        if(strcmp(leaf, node->data) == 0){
            return node;
        }
        if(node->left != nullptr){
            auto left = this->find(node->left, leaf);
            if(left != nullptr)
                return left;
        }
        if(node->right != nullptr){
            auto right = this->find(node->right, leaf);
            if(right != nullptr)
                return right;
        }
        return nullptr;
    }

    //  find parent of a node
    Node* find_parent(Node *parent, Node *node, char *leaf){
        if(node == nullptr){
            return nullptr;
        }
        if(strcmp(leaf, node->data) == 0){
            return parent;
        }
        if(node->left != nullptr){
            auto left = this->find_parent(node, node->left, leaf);
            if(left != nullptr)
                return left;
        }
        if(node->right != nullptr){
            auto right = this->find_parent(node, node->right, leaf);
            if(right != nullptr)
                return right;
        }
        return nullptr;
    }

    // Print lexicographically
    void print(Node *parent){
        printf("(%s", parent->data);
        if(parent->left != nullptr && parent->right != nullptr){
            if(parent->left->data < parent->right->data){
                this->print(parent->left);
                this->print(parent->right);
            }
            else{
                this->print(parent->right);
                this->print(parent->left);
            }
        }
        else if(parent->left != nullptr){
            this->print(parent->left);
        }
        else if(parent->right != nullptr){
            this->print(parent->right);
        }
        printf(")");
    }

};

bool is_valid_input(const std::string& s)
{
    return (!std::isspace(static_cast<unsigned char>(s.front())) &&
            !std::isspace(static_cast<unsigned char>(s.back())));
}

void parse_input_tree(const char *input_tree){
    auto btree = new Btree();
    const char *begin = input_tree;
    const char *end = (input_tree + strlen(input_tree));
    bool is_valid = false;
    const char *processed_until = begin;

    if(strncmp(begin, "(", 1) != 0 || strncmp(end-1, ")", 1) != 0){ // skip processing if garbage string is provided
        printf("E1"); // Input Format error
        ::exit(0);
    }

    // traverse once and try to parse parent child pairs
    for(char *curr = const_cast<char *>(begin); curr < end; ++curr){
        if(strncmp(curr, &TAG_OPEN, 1) == 0){ // handle = in fix message section
            is_valid = true;
            *curr = NULL_CHAR;
            processed_until = curr+1;
        }
        if(strncmp(curr, &NODE_DELIMITER, 1) == 0){ // handle = in fix message section
            if(is_valid){
                *curr = NULL_CHAR;
                parent = processed_until;
                processed_until = curr+1;
            }
        }
        if(strncmp(curr, &TAG_CLOSE, 1) == 0){ // handle = in fix message section
            if(is_valid){
                *curr = NULL_CHAR;
                child = processed_until;
                processed_until = curr+1;
                if(curr+1 != end){
                    if(strncmp(curr+2, "(", 1) != 0){
                        printf("E1");
                        ::exit(0);
                    }
                }
            }
        }
        if(is_valid && (strncmp(parent, &NULL_CHAR, 1) !=0) && (strncmp(child, &NULL_CHAR, 1) !=0)){
            is_valid = false;
            // insert parent & child to Btree
            //check if parent is present
            if(btree->root == nullptr){
                // insert parent
                btree->insert_root(const_cast<char *>(parent));
            }
            btree->insert_node(const_cast<char *>(parent), const_cast<char *>(child));
            parent = &NULL_CHAR;
            child = &NULL_CHAR;
        }
    }
    if(btree->root == nullptr){
        printf("E1");
        ::exit(0);
    }
    else{
        btree->print(btree->root);
    }
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    std::string input_tree;

    std::getline(std::cin, input_tree);
    if(is_valid_input(input_tree)){
        parse_input_tree(input_tree.c_str());
    }
    else{
        printf("E1"); // invalid input format
    }


    return 0;
}