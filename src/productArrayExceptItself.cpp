//
// Created by nikhil on 2/27/23.
//

//
// Created by nikhil on 2/27/23.
//


#include <string>
#include "iostream"
#include "vector"


class Solution {
public:
    void productArrayExceptItself(std::vector<int> &n) {
        std::vector<int> v(n.size()+1, 1);
        int i=0;
        int temp=1;
        while(i < n.size()){
            v[i] = temp;
            temp = temp * n[i];

            i++;
        }
        temp = 1;
        i--;
        while(i >= 0){
            v[i] = v[i] * temp;
            temp = temp * n[i];
            i--;
        }
        for(auto elem:v){
            std::cout << elem << std::endl;
        }

    }
};
int main(){
    auto solution = new Solution();
    std::vector<int> nums = {10,3,5,6,2};
    solution->productArrayExceptItself(nums);
    return 0;
}