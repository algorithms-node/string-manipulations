//
// Created by nikhil on 2/27/23.
//

//
// Created by nikhil on 2/27/23.
//


#include <string>
#include "iostream"
#include "vector"
#include "unordered_set"

class Solution {
public:
    // first approach
    double findMedianSortedArrays(std::vector<int>& nums1, std::vector<int>& nums2) {
        std::vector<int> combined{};
        uint16_t i=0, j=0;
        while(i < nums1.size() && j < nums2.size()){
            if(i< nums1.size()){
                if(nums1[i] < nums2[j]){
                    combined.push_back(nums1[i]);
                    i++;
                }

            }
            if(j< nums2.size()){
                if(nums2[j] < nums1[i]){
                    combined.push_back(nums2[j]);
                    j++;
                }
            }
        }
        while(i < nums1.size()){
            combined.push_back(nums1[i]);
            i++;
        }
        while(j < nums2.size()){
            combined.push_back(nums2[j]);
            j++;
        }
        uint16_t comb_size = combined.size();
        if(comb_size == 0){
            std::cout << 0 << std::endl;
            return 0;
        }
        else if(comb_size % 2 == 0){
            std::cout << (combined[comb_size/2] + combined[comb_size/2 -1])/2.0 << std::endl;
            return (combined[comb_size/2] + combined[comb_size/2 -1])/2.0;
        }
        else{
            std::cout << combined[comb_size/2.0] << std::endl;
           return  combined[comb_size/2.0];
        }
    }
};
int main(){
    auto solution = new Solution();
    std::vector<int> v1 = {};
    std::vector<int> v2 = {};
    solution->findMedianSortedArrays(v1, v2);
    return 0;
}



